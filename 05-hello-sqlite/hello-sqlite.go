package main

import (
	"fmt"
	"net/http"
	"log"
	"database/sql"
)

var (
	db *sql.DB
	dberr error
)

func main() {
	db, dberr = sql.Open("sqlite3",
		"hello_go.db")
	if dberr != nil {
		fmt.Println("Connect to database error.")
		fmt.Println(dberr)
		return
	}
	http.HandleFunc("/", handleRoot)
	http.HandleFunc("/login", handleLogin)
	http.HandleFunc("/upload", handleUpload)
	http.HandleFunc("/sqlite", handleSqlite)
	fmt.Println("Start server")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

	return
}

