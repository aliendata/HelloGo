package main

import (
	"fmt"
	"time"
	"strconv"
	"crypto/md5"
	"net/http"
	"html/template"
)

func securePrint(w http.ResponseWriter, args ...interface{}) (int, error) {
	var str string = ""
	for _, v := range args {
		str = fmt.Sprint(str, v)
	}
//	return fmt.Fprintln(w, template.HTMLEscaper(str))
	return fmt.Fprintln(w, "<pre>" + template.HTMLEscaper(str) + "</pre>")
}

func printEmptyHeader(w http.ResponseWriter, title string) {
	fmt.Fprintln(w, `
<html>
  <head>
    <title>` + title + `</title>
  </head>
  <body>
	`)
}

func printEmptyFooter(w http.ResponseWriter) {
	fmt.Fprintln(w, `
  </body>
</html>
	`)
}

func genToken() (int64, string) {
	unixtime := time.Now().Unix()
	hashToken := md5.New()
	fmt.Fprint(hashToken, strconv.FormatInt(unixtime, 10))
	token := fmt.Sprintf("%x", hashToken.Sum(nil))
	return unixtime, token
}

