<html>
  <head>
    <title>Upload</title>
  </head>
  <body>
    <h2>Upload</h2>
    <form enctype="multipart/form-data" action="/upload" method="post">
      <input type="file" name="uploadfile" />
      <input type="hidden" name="token" value="{{.token}}" />
      <input type="submit" value="upload">
    </form>
  </body>
</html>

