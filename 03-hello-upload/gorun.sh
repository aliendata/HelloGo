#!/bin/bash

BIN="hello-upload.run"
SRC="hello-upload.go handler.go functions.go"

if [[ -d "`pwd`/../src" ]]; then
	export GOPATH="`pwd`:`pwd`/.."
else
	export GOPATH="`pwd`"
fi

rm -f ${BIN}
go build -o ${BIN} ${SRC}

if [[ $? == 0 ]]; then
	echo "=> compile finished." > /dev/stderr
	time ./${BIN} "$@"
else
	echo "=> compile failed, abort." > /dev/stderr
fi
