package main

import (
	"fmt"
	"net/http"
)

func handleRoot(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Println("DEBUG:\n", "Request:\t", *r)
	fmt.Fprintln(w, "Request:\t", *r)
	fmt.Fprintln(w, "Method:\t", r.Method)
	fmt.Fprintln(w, "Host:\t", r.Host)
	fmt.Fprintln(w, "Proto:\t", r.Proto)
	fmt.Fprintln(w, "URL:\t", r.URL)
	fmt.Fprintln(w, "Scheme:\t", r.URL.Scheme)
	fmt.Fprintln(w, "Path:\t", r.URL.Path)
	fmt.Fprintln(w, "Header:")
	for k, v := range r.Header {
		fmt.Fprintln(w, k, "=>", v)
	}
	fmt.Fprintln(w, "Parameter:")
	for k, v := range r.Form {
		fmt.Fprintln(w, k, "=>", v)
	}
	fmt.Fprintln(w, "\nHtml output finished.")
	return
}

func handleAPI(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Fprintln(w, "Request:", *r)
	fmt.Fprintln(w, "Hello handleAPI")
	return
}
