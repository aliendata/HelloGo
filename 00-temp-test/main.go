// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"

	"github.com/go-playground/validator/v10"
	jsoniter "github.com/json-iterator/go"
)

type User struct {
	Name string `json:"name" validate:"required"`
	Age  int8   `json:"age" validate:"required"`
}

var (
	json     jsoniter.API
	validate *validator.Validate
)

func init() {
	validate = validator.New()
	json = jsoniter.Config{DisallowUnknownFields: true}.Froze()
}

func main() {
	testCases := []string{
		`{"name": "john", "age": 10}`,
		`{"name": "john", "age": 10, "is_v2ex": false}`,
		`{"name": "john"}`,
		`{"name": "john", "age": null}`,
		`{"name": "john", "age": 100000000}`,
	}

	for _, tc := range testCases {
		tmp := &User{}
		err := Unmarshal([]byte(tc), tmp)
		fmt.Printf("testCase: %s, result: %v\n", tc, err)
	}
}

func Unmarshal(data []byte, v any) error {
	if err := json.Unmarshal(data, v); err != nil {
		return err
	}
	return validate.Struct(v)
}
