package main

import (
	"net"
)

func namelookup(name, server string) error {
	_, err := net.LookupHost(name)
	if err != nil {
		return (err)
	}
	return nil
}
