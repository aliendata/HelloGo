package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"
)

var (
	fileIn    string
	dnsServer string
)

func main() {
	flag.StringVar(&fileIn, "f", "", "file for names, line by line.")
	flag.StringVar(&dnsServer, "s", "", "dns server.")
	flag.Parse()

	var (
		f  *os.File
		rd *bufio.Reader
	)
	f, err := os.Open(fileIn)
	if err != nil {
		log.Fatal("open data file ", fileIn, " failed")
	}
	defer f.Close()

	rd = bufio.NewReader(f)
	time1 := time.Now()
	for {
		line, err := rd.ReadString('\n')
		if err != nil || io.EOF == err {
			break
		}
		name := strings.Split(line, " ")[0]
		namelookup(name, dnsServer)
	}
	time2 := time.Now()
	timeDelta := time2.Sub(time1)
	fmt.Println(timeDelta.String())
}
