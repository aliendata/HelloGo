package main

import (
	"log"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
)

func main() {
	log.Println("Connecting to server...")

	sbox := "pp_zabbix_alert"

	// Connect to server
	c, err := client.Dial("imap.suning.com:143")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected")

	// Don't forget to logout
	defer c.Logout()

	// Login
	if err := c.Login("16091670@suning.com", "Tda<e{eJCj9qkm3V"); err != nil {
		log.Fatal(err)
	}
	log.Println("Logged in")

	// List mailboxes
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- c.List("", "*", mailboxes)
	}()

	log.Println("Mailboxes:")
	for m := range mailboxes {
		log.Println("* " + m.Name)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	// Select INBOX
	mbox, err := c.Select(sbox, false)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Flags for", sbox, mbox.Flags)

	// Get the last 4 messages
	from := uint32(1)
	to := mbox.Messages
	log.Println("DEBUG: mbox:", mbox)
	if mbox.Messages > 3 {
		// We're using unsigned integers here, only substract if the result is > 0
		from = mbox.Messages - 3
	}
	from = 1
	to = 0
	log.Println("DEBUG: mbox.Messages:", mbox.Messages)
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)
	log.Println("DEBUG: seqset Range:", seqset.Set)

	messages := make(chan *imap.Message, 10)
	done = make(chan error, 1)
	go func() {
		mbox.ItemsLocker.Lock()
		done <- c.Fetch(seqset, []imap.FetchItem{imap.FetchEnvelope}, messages)
		mbox.ItemsLocker.Unlock()
	}()

	log.Println("Last", len(messages), "messages:")
	for msg := range messages {
		log.Println("* " + msg.Envelope.Subject)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	log.Println("Done!")
}

// https://github.com/emersion/go-imap
