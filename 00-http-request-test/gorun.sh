#!/bin/bash

BIN="http-request-test.run"
SRC="http-request-test.go handler.go options.go"

if [[ -d "$(pwd)/../pkg" ]]; then
	export GOPATH="$(pwd)/.."
elif [[ -d "$(pwd)/pkg" ]]; then
	export GOPATH="$(pwd)"
else
	mkdir -p $(xdg-user-dir)/.cache/go
	export GOPATH="$(xdg-user-dir)/.cache/go"
fi

rm -f ${BIN}
CGO_ENABLED=0 go build -ldflags='-w -s' -o ${BIN} ${SRC}

if [[ $? == 0 ]]; then
	echo "=> compile finished." > /dev/stderr
	time ./${BIN} "$@"
else
	echo "=> compile failed, abort." > /dev/stderr
fi
