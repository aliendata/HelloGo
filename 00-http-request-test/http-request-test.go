package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

var (
	serv_http string
	backend   string
)

func main() {
	flag.StringVar(&serv_http, "http", ":8080",
		"HTTP service address.")
	flag.StringVar(&backend, "backend", "127.0.0.1:80",
		"Backend HTTP server.")
	flag.Parse()

	http.HandleFunc("/", handleRoot)
	fmt.Println("Start server")
	err := http.ListenAndServe(serv_http, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
